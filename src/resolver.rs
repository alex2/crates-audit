use std::collections::{BTreeMap as Map, BTreeSet as Set, HashMap, VecDeque};
use std::fmt;
use std::iter::FromIterator;

use semver::VersionReq;

use index::*;

pub enum ResolveError {
    SatisfyDep(Dependency),
}

impl fmt::Display for ResolveError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ResolveError::SatisfyDep(dep) => {
                writeln!(f, "failed to find crate that satisfies `{}`", dep)
            }
        }
    }
}

/// Specification for a dependency that needs to be filled.
#[derive(Debug, PartialEq, Eq, Hash)]
pub struct Dependency {
    /// Name of the crate that is required.
    pub name: String,
    /// Version requirements of this dependency.
    pub req: VersionReq,
    /// The explicitly activated features.
    pub features: Set<String>,
    /// Whether the default features are desired.
    pub default_features: bool,
}

impl fmt::Display for Dependency {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.name, self.req)
    }
}

impl<'a> From<&'a IndexEntryDep> for Dependency {
    fn from(index_dep: &IndexEntryDep) -> Self {
        Dependency {
            name: index_dep.name.clone(),
            req: VersionReq::parse(&index_dep.req)
                .expect("failed to parse index entry requirement"),
            features: Set::from_iter(index_dep.features.iter().cloned()),
            default_features: index_dep.default_features,
        }
    }
}

fn resolve_step(
    upstream_crates_index: &IndexCache,
    dep: Dependency,
    deps_to_resolve: &mut VecDeque<Dependency>,
    resolved_deps: &mut HashMap<Dependency, IndexEntry>,
) -> Result<(), ResolveError> {
    let index_entry = match upstream_crates_index.get_required(&dep.name, &dep.req) {
        Some(v) => v,
        None => return Err(ResolveError::SatisfyDep(dep)),
    };

    let mut activated_features: Set<String> = Set::from_iter(dep.features.iter().cloned());
    if dep.default_features {
        if let Some(default_features) = index_entry.features.get("default") {
            if !default_features.is_empty() {
                activated_features.extend(default_features.iter().cloned());
            }
        }
    }

    let mut do_feature_loop = true;
    while do_feature_loop {
        do_feature_loop = false;
        for (feature, sub_features) in &index_entry.features {
            if !activated_features.contains(feature) {
                continue;
            }
            let old_activated_features_len = activated_features.len();
            activated_features.extend(sub_features.iter().cloned());
            if old_activated_features_len != activated_features.len() {
                do_feature_loop = true;
            }
        }
    }

    // To simplify reexported feature activation (e.g. subcrate-foo/feature-bar), this creates a map
    // from sub-crate name to feature.
    let mut activated_reexported_features: Map<&str, Set<&str>> = Map::new();
    for feature in &activated_features {
        let mut feature_split = feature.splitn(2, '/');
        if let (Some(crate_name), Some(reexported_feature)) =
            (feature_split.next(), feature_split.next())
        {
            activated_reexported_features
                .entry(crate_name)
                .or_insert_with(|| Default::default())
                .insert(reexported_feature);
        }
    }

    for index_entry_dep in &index_entry.deps {
        if index_entry_dep
            .kind
            .as_ref()
            .map(|kind| kind == "dev")
            .unwrap_or(true)
        {
            continue;
        }

        if index_entry_dep.optional && !activated_features.contains(&index_entry_dep.name) {
            continue;
        }

        let mut sub_dep = Dependency::from(index_entry_dep);
        if let Some(reexported_features) =
            activated_reexported_features.get(index_entry_dep.name.as_str())
        {
            for &reexported_feature in reexported_features {
                if !sub_dep.features.contains(reexported_feature) {
                    sub_dep.features.insert(reexported_feature.to_owned());
                }
            }
        }

        if !resolved_deps.contains_key(&sub_dep) {
            deps_to_resolve.push_back(sub_dep);
        }
    }

    resolved_deps.insert(dep, index_entry.clone());

    Ok(())
}

pub fn resolve_deps(
    crates_index: &IndexCache,
    entry: &IndexEntry,
) -> Result<Vec<IndexEntry>, ResolveError> {
    let mut deps_to_resolve = VecDeque::new();
    for dep in &entry.deps {
        deps_to_resolve.push_back(dep.into());
    }
    let mut resolved_deps = HashMap::new();
    loop {
        let dep = match deps_to_resolve.pop_front() {
            Some(dep) => dep,
            None => break,
        };
        resolve_step(crates_index, dep, &mut deps_to_resolve, &mut resolved_deps)?;
    }
    Ok(resolved_deps.into_iter().map(|(_, v)| v).collect())
}
